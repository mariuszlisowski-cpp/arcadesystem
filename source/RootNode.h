#ifndef ROOTNODE_H
#define ROOTNODE_H

#include "Action.h"
#include "ArcadeTexture.h"
#include "Node.h"
#include "Config.h"
#include "MenuScreen.h"
#include "SDL_render.h"
#include "SimpleButton.h"

class RootNode : public Node
{
public:
    RootNode(SDL_Renderer* renderer_in, Node* parentNode_in)
        : Node(renderer_in, parentNode_in)
    {
        /* screens */
        MenuScreen* screen1 = createMenuScreen();
        MenuScreen* screen2 = createMenuScreen();

        /* backgournds */
        ArcadeTexture* screen1Background =
            createImage(renderer_in, "rootNodeImages/rootNodeScreenBackground.png", true);
        ArcadeTexture* screen2Background =
            createImage(renderer_in, "rootNodeImages/rootNodeScreenBackground.png", true);

        /* texts */
        ArcadeTexture* screen1Text = 
            createSimpleText(renderer_in, "fonts/retro/italic.ttf", 100, "screen 1", 255, 255, 0);
        screen1Text->setPosition(windowWidth / 2 - windowHeight / 2, 25);

        ArcadeTexture* screen2Text = 
            createSimpleText(renderer_in, "fonts/retro/italic.ttf", 100, "screen 2", 255, 255, 0);
        screen2Text->setPosition(windowWidth / 2 - windowHeight / 2, 25);

        screen1->addTextureToScreen(screen1Background);
        screen1->addTextureToScreen(screen1Text);

        screen2->addTextureToScreen(screen2Background);
        screen2->addTextureToScreen(screen2Text);

        /* buttons */
        SimpleButton* button1 =
            createSimpleTextButton(renderer_in, "fonts/pixel/classic.ttf", 30, "gotoscreen2", 255, 0, 0);
        button1->setButtonPosition(windowWidth / 2 - button1->getWidth() / 2,
                                   screen1Text->getY() + screen1Text->getHeight() + 50);
        button1->setButtonAction(createAction(MOVE_SCREENS, screen2));

        SimpleButton* button2 =
            createSimpleTextButton(renderer_in, "fonts/pixel/classic.ttf", 30, "gotoscreen1", 255, 0, 0);
        button2->setButtonPosition(windowWidth / 2 - button2->getWidth() / 2,
                                   screen2Text->getY() + screen2Text->getHeight() + 50);
        button2->setButtonAction(createAction(MOVE_SCREENS, screen1));

        screen1->addButtonToScreen(button1);
        screen2->addButtonToScreen(button2);

        /* update node */
        this->addScreen(screen1);
        this->addScreen(screen2);
        this->setCurrentScreen(screen1);
    }
};

#endif // !ROOTNODE_H